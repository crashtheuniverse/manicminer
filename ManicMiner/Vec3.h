//
//  Vec3.h
//  ManicMiner
//
//  Created by CrashTheuniversE on 23/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#ifndef ManicMiner_Vec3_h
#define ManicMiner_Vec3_h

#include <cmath> 

namespace Engine {

struct Vec3 {
	
	Vec3() {};
	Vec3(float vx, float vy, float vz) : x(vx), y(vy), z(vz) {};
	explicit Vec3(float s) : x(s), y(s), z(s) {};
	
	union
	{
		struct
		{
			float x,y,z;
		};
		struct
		{
			float r,g,b;
		};
		float m[3];
	};
	
	Vec3 operator+(const Vec3& r) const {
		
		return Vec3(x + r.x, y + r.y, z + r.z);
	};
	Vec3 operator-(const Vec3& r) const {
		
		return Vec3(x - r.x, y - r.y, z - r.z);
	};
	Vec3 operator-() const {
		return Vec3(-x, -y, -z);
	};
	Vec3 operator*(float s) const {
		return Vec3(x * s, y * s, z * s);
	};
	Vec3 operator/(float s) const {
		float inv = 1.0f / s;
		return Vec3(x * inv, y * inv, z * inv);
	};
	
	Vec3& operator+=(const Vec3& r) {
		x += r.x;
		y += r.y;
		z += r.z;
		return *this;
	}
	
	Vec3& operator-=(const Vec3& r) {
		x -= r.x;
		y -= r.y;
		z -= r.z;
		return *this;
	}
	
	
	float lengthSquared() {
		return (x*x + y*y + z*z);
	}
	
	float length() {
		return sqrt(lengthSquared());
	}
	
	Vec3 getNormalized() {
		return (*this / length());
	}
	
	void normalize() {
		*this = getNormalized();
	}
    
    //static const Vec3 zero;
};

    //const Vec3 Vec3::zero = { 0.0f, 0.0f, 0.0f };
    
}

#endif
