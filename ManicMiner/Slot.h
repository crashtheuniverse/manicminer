//
//  Slot.h
//  ManicMiner
//
//  Created by CrashTheuniversE on 24/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#ifndef __ManicMiner__Slot__
#define __ManicMiner__Slot__

#include "Rect.h"
#include "Element.h"

class Slot {
  
    Engine::Rect aabb;
    Element * element;
    int tx, ty;
    bool killMark;
    
public:

    Slot(int x, int y) : element(NULL), tx(x), ty(y), killMark(false) {};
    ~Slot();
    
    void AABB(const Engine::Rect& r) { aabb = r; };
    const Engine::Rect& AABB() const { return aabb; };
    
    void setElement(Element * e);
    Element * getElement() const { return element; };
    
    bool empty() const { return element == NULL; };
    
    int x() const { return tx; };
    int y() const { return ty; };
    
    void killmark(bool mark) { killMark = mark; };
    bool killmark() const { return killMark; };
    
    bool sameElement(const Slot * const e) const
    {
        return ( !e->empty() && !this->empty() && (e->getElement()->type() == getElement()->type()) );
    };
    
    void swapElement(Slot * dstSlot);
};



#endif /* defined(__ManicMiner__Slot__) */
