//
//  Sprite.h
//  ManicMiner
//
//  Created by CrashTheuniversE on 23/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#ifndef ManicMiner_Sprite_h
#define ManicMiner_Sprite_h

#include "Entity.h"
#include "Vec3.h"
#include "Rect.h"
#include <SDL2/SDL.h>
#include <vector>
#include "AnimationController.h"

namespace Engine {

    struct Environment;
    class Texture;
    
class Sprite : public Entity {

    typedef std::vector<Vec3AnimController*> AnimContainer;
    
    Vec3 pos;
    float width;
    float height;
    Texture * texture;
    
    AnimContainer animations;
    
public:
    
    Sprite() : pos(Vec3(0,0,0)), width(1), height(1), texture(NULL) {};
    virtual ~Sprite() {};
    
    virtual void update(Engine::Environment& env);
    
    const Vec3& position() const { return pos; };
    void position(const Vec3& p) { pos = p; };
    void translate(const Vec3& t) { pos += t; };
    
    void moveTo(const Vec3& d);
    
    void size(float w, float h) { width = w, height = h; };
    
    const Rect AABB() { return Rect(pos.x, pos.y, width, height); };
    
    void setTexture(Texture * t);
    Texture * getTexture() { return texture; };
    
    bool isAnimating() { return !animations.empty(); };
};

}

#endif
