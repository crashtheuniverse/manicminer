//
//  main.cpp
//  ManicMiner
//
//  Created by CrashTheuniversE on 23/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

//ToDo:
// Improve random generators
// AnimKey to link for movement to sprites

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include <OpenGL/gl.h>

#include "Environment.h"
#include "Renderer.h"
#include "Rect.h"
#include "Game.h"

void gameInit(Engine::Environment& env);
void gameUpdate(Engine::Environment& env, float deltaTimeMillis);
void gameUpdateInput(Engine::Environment& env);
void gameExit(Engine::Environment& env);

const int FPS = 60;
const int MS_FRAME = 1000 / FPS;

int main(int argc, const char * argv[])
{
    Engine::Environment env;
    gameInit(env);
    
    //While the user hasn't quit
    while( env.quit == false )
    {
        //FRAME BEGIN
        int frameBegin = SDL_GetTicks();
        
        gameUpdateInput(env);
        
        gameUpdate(env, MS_FRAME);
        
        env.renderer.renderFrame();
        
        int frameEnd = SDL_GetTicks();
        
        //FRAME END
        
        if( (frameEnd - frameBegin) < MS_FRAME )
        {
            SDL_Delay( MS_FRAME - (frameEnd - frameBegin));
        }
        
        env.renderer.swap();
    }
    
}

void gameInit(Engine::Environment& env)
{
    env.renderer.init(1024, 768);
    env.getTextureManager()->setPath("/Users/crashtheuniverse/imgs/");

    Game * game = new Game();
    env.addEntity(game);
    game->init(env);
}

void gameExit(Engine::Environment& env)
{
}

void gameUpdate(Engine::Environment& env, float deltaTimeMillis)
{
    env.deltaTime = deltaTimeMillis * 0.001f;
    
    for( Engine::Environment::EntityContainer::iterator it = env.entities.begin();
        it != env.entities.end();
        ++it)
    {
        (*it)->update(env);
    }
}

void gameUpdateInput(Engine::Environment& env)
{
    SDL_Event event;
    
    env.inputEvents.clear();
    
    //While there's an event to handle
    while( SDL_PollEvent( &event ) )
    {
        env.inputEvents.push_back(event);
        
        if( event.type == SDL_KEYDOWN )
        {
            switch( event.key.keysym.sym )
            {
                case SDLK_1 : { env.quit = true; break; }
                default: break;
            }
        }
        
        //If the user has Xed out the window
        if( event.type == SDL_QUIT )
        {
            //Quit the program
            env.quit = true;
        }
    }
}