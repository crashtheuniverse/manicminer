//
//  Rect.h
//  ManicMiner
//
//  Created by CrashTheuniversE on 23/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#ifndef ManicMiner_Rect_h
#define ManicMiner_Rect_h

namespace Engine {
    
    template<typename T>
    struct TRect {
        
        TRect() {};
        TRect(T vx, T vy, T vw, T vh) : x(vx), y(vy),w(vw),h(vh) {};
        
        union
        {
            struct
            {
                T x,y,w,h;
            };
            T m[4];
        };
        
        TRect operator*(T s) const {
            return Rect(x, y, w * s, h * s);
        };
        
        int pointIn(T px, T py) const {
            if( (px > x) && (px < (x + w)) )
            {
                if( (py > y) && (py < (y + h)) )
                {
                    return 1;
                }
            }
            
            return 0;
        }
    };
    
    typedef TRect<float> Rect;
}

#endif
