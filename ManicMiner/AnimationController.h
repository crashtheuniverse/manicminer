//
//  AnimationController.h
//  ManicMiner
//
//  Created by CrashTheuniversE on 25/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

//CTU: This should have been a "linear" controller

#ifndef ManicMiner_AnimationController_h
#define ManicMiner_AnimationController_h

namespace Engine {

    template <typename T>
    struct TAnimationController {
        
        T& value;
        T start;
        T target;
        T delta;
        float time;
        float currTime;
        
        TAnimationController(T& v, const T& targetValue, float totalTime) :
        value(v),
        start(v),
        target(targetValue),
        time(totalTime),
        currTime(0)
        {
            delta = (target - value) * (1.0f / time);
        };
        
        void update(float dt)
        {
            value += delta * dt;
            currTime += dt;
            
            if(currTime > time) {
                currTime = time;
                value = target;
            }
        }
        
        T valueAt(float t)
        {
            if(t <= 0.0f)
                return start;
            if(t >= time)
                return target;
            
            return start + (t * delta);
        }
        
        bool complete() { return currTime >= time; };
    };
    
    typedef TAnimationController<Vec3> Vec3AnimController;
}

#endif
