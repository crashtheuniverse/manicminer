//
//  Slot.cpp
//  ManicMiner
//
//  Created by CrashTheuniversE on 24/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#include "Slot.h"
#include "Element.h"
#include "Sprite.h"
#include <iostream>

void Slot::setElement(Element * e)
{
    //CTU: Change the position of the element to the one of the slot
    element = e;
    if(e != NULL)
    {
        e->moveTo( Engine::Vec3(aabb.x, aabb.y, 0.0f) );
    }
}

void Slot::swapElement(Slot * dstSlot)
{
    Element * de = dstSlot->getElement();
    dstSlot->setElement(element);
    setElement(de);
}

Slot::~Slot()
{
    std::cout << " SLOT DESTROY " << std::endl;
}