//
//  Game.cpp
//  ManicMiner
//
//  Created by CrashTheuniversE on 23/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#include "Game.h"
#include "Element.h"
#include "Environment.h"
#include "Board.h"
#include <iostream>

using namespace Engine;

const int BOARD_BLOCKS_COUNT = 8;
const int BOARD_BLOCKS_SIZE = 60;
const int BOARD_POS_X = 430;
const int BOARD_POS_Y = 134;
const int BOARD_BLOCK_TYPES = 5;

Game::Game() :
board(NULL)
{
}

Game::~Game()
{
    delete board;
}

void Game::init(Engine::Environment& env)
{
    board = new Board(BOARD_BLOCKS_COUNT, BOARD_BLOCKS_COUNT, BOARD_BLOCKS_SIZE, BOARD_BLOCK_TYPES);
    board->init(env);
    board->setPosition(BOARD_POS_X, BOARD_POS_Y);
    env.addEntity(board);
    
    Sprite * bg = new Sprite();
    Texture * bgTex = env.getTextureManager()->getTexture("bg.png");
    bg->setTexture(bgTex);
    
    env.renderer.addSprite(bg);
}

void Game::update(Engine::Environment& env)
{
    //CTU: May want to defer the input processing inside the board itself
    
    for(size_t i = 0, s = env.inputEvents.size(); i < s; ++i)
    {
        SDL_Event evt = env.inputEvents[i];
        if(evt.type == SDL_MOUSEBUTTONDOWN)
        {
            std::cout << "MX: " << evt.button.x << " MY:" << evt.button.y << std::endl;
        }
        
        if(evt.type == SDL_MOUSEBUTTONUP)
        {
            std::cout << "MX: " << evt.button.x << " MY:" << evt.button.y << std::endl;
        }
    }
}