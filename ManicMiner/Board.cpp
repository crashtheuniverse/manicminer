//
//  Board.cpp
//  ManicMiner
//
//  Created by CrashTheuniversE on 23/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#include "Board.h"
#include "Renderer.h"
#include "Environment.h"
#include "Element.h"
#include "Rect.h"
#include <sstream>

using namespace Engine;

Board::Board(int w, int h, int blocksize, int types) :
width(w),
height(h),
blockSize(blocksize),
maxTypes(types),
srcSlot(NULL),
dstSlot(NULL),
swapBack(false),
swap(false)
{}

void Board::init(Engine::Environment &env)
{
    slots.resize(width * height, NULL);
    
    for(int j = 0; j < height; ++j)
    {
        for(int i = 0; i < width; ++i)
        {
            Slot * ss = new Slot(i, j);
            ss->AABB( Rect(i * blockSize, j * blockSize, blockSize, blockSize) );
            slots[(j * width) + i] = ss;
        }
    }
}

void Board::update(Engine::Environment& env)
{
    for(size_t i = 0, s = env.inputEvents.size(); i < s; ++i)
    {
        SDL_Event evt = env.inputEvents[i];
        if(evt.type == SDL_MOUSEBUTTONDOWN)
        {
            srcSlot = getSlotAt(evt.button.x, evt.button.y);
        }
        
        if(evt.type == SDL_MOUSEBUTTONUP)
        {
            //FIXME: Can swap with anything, should only get an adjacent one
            dstSlot = getSlotAt(evt.button.x, evt.button.y);
            
            if(srcSlot != dstSlot && (srcSlot != NULL) && (dstSlot != NULL))
            {
                srcSlot->swapElement(dstSlot);
                swap = true;
            }
            else
            {
                //CTU: do nothing (?)
            }
        }
    }
    
    
    if(swap)
    {
        if(hasElementsInMovement())
        {
        }
        else
        {
            swap = false;
            calcSlotsToDestroyMatch3();
            swapBack = !destroySlots();
        }
    }
    else
    {
        if(swapBack)
        {
            if(srcSlot != NULL && dstSlot != NULL)
            {
                srcSlot->swapElement(dstSlot);
                srcSlot = dstSlot = NULL;
                swapBack = false;
            }
        }
        else if(hasEmptySlots())
        {
            if(!hasElementsInMovement())
            {
                //CTU: Drag down pieces
                dragDown();
                
                //CTU: Check if top row has any empty and fill
                spawnEmpty(env);
            }
        }
        else
        {
            calcSlotsToDestroyMatch3();
            destroySlots();
        }
    }
}


bool Board::hasElementsInMovement()
{
    for(SlotContainer::iterator it = slots.begin(); it != slots.end(); ++it)
    {
        Element * e = (*it)->getElement();
        
        if( (e != NULL) && e->isAnimating())
            return true;
    }
    
    return false;
}

bool Board::destroySlots()
{
    bool bDestroy = false;
    if(slotsToDestroy.size() >= 3)
    {
        bDestroy = true;
    }
    
    for(SlotContainer::iterator it = slotsToDestroy.begin();
        it != slotsToDestroy.end();
        ++it)
    {
        if(bDestroy)
        {
            Element * e = (*it)->getElement();
            e->moveTo(Vec3(0.0f, 0.0f, 0.0f));
            (*it)->setElement(NULL);
        }
        (*it)->killmark(false);
    }
    
    slotsToDestroy.clear();
    
    return bDestroy;
}

int Board::rowSlotsToDestroy(int start, int row, SlotContainer& toDestroy)
{
    int i = start;
    toDestroy.push_back(getSlot(i, row));
    for(; i < (width - 1); ++i)
    {
        if( getSlot(i, row)->getElement()->type() == getSlot(i + 1, row)->getElement()->type() )
        {
            toDestroy.push_back(getSlot(i + 1, row));
        }
        else
        {
            break;
        }
    }
    
    return (i+1);
}

int Board::columnSlotsToDestroy(int start, int col, SlotContainer& toDestroy)
{
    int j = start;
    toDestroy.push_back(getSlot(col, j));
    for(; j < (height - 1); ++j)
    {
        if( getSlot(col, j)->getElement()->type() == getSlot(col, j+1)->getElement()->type() )
        {
            toDestroy.push_back(getSlot(col, j+1));
        }
        else
        {
            break;
        }
    }
    
    return (j+1);
}

void Board::pushSlotsToDestroy(SlotContainer& toDestroy)
{
    //CTU: May want to add score calculation here?
    
    for(SlotContainer::iterator it = toDestroy.begin(); it != toDestroy.end(); ++it)
    {
        if( !(*it)->killmark() )
        {
            (*it)->killmark(true);
            slotsToDestroy.push_back(*it);
        }
    }
}

void Board::calcSlotsToDestroyMatch3()
{
    //CTU: Horizontal lines
    for(int j = 0; j < this->height; ++j)
    {
        int x = 0;
        while(x < (width - 1))
        {
            SlotContainer toDestroy;
            x = rowSlotsToDestroy(x, j, toDestroy);
            if(toDestroy.size() >= 3) {
                pushSlotsToDestroy(toDestroy);
            }
        }
    }

    //CTU: Vertical lines
    for(int i = 0; i < this->width; ++i)
    {
        int y = 0;
        while(y < (height - 1))
        {
            SlotContainer toDestroy;
            y = columnSlotsToDestroy(y, i, toDestroy);
            if(toDestroy.size() >= 3) {
                pushSlotsToDestroy(toDestroy);
            }
        }
    }
}

bool Board::hasEmptySlots()
{
    for(SlotContainer::iterator it = slots.begin();
        it != slots.end();
        ++it)
    {
        if( (*it)->empty())
            return true;
    }
    
    return false;
}


Slot * Board::getSlot(int x, int y)
{
    if( (x >= 0) && ( x < width ) && ( y >= 0 ) && ( y < height ) )
    {
        return slots[ (y * width) + x];
    }
    return NULL;
}

void Board::dragDown()
{
    for(int j = (height - 1) ; j > 0; --j)
    {
        for(int i = 0; i < width; ++i)
        {
            Slot * d = getSlot(i, j);
            Slot * s = getSlot(i, j - 1);
            
            if(d->empty())
            {
                d->setElement( s->getElement() );
                s->setElement(NULL);
            }
        }
    }
}

void Board::spawnEmpty(Engine::Environment& env)
{
    using namespace Engine;
    
    for(int i = 0; i < width; ++i)
    {
        Slot * s = slots[i];
        
        if(s->empty())
        {
            Rect r = s->AABB();
            
            int type = rand() % maxTypes;
            
            std::stringstream ss;
            ss << type << ".png";
            Texture * t = env.getTextureManager()->getTexture(ss.str());
            
            Sprite * spr = new Sprite();
            spr->position( Vec3(r.x, r.y - r.h, 0.0f) );
            spr->setTexture(t);
            env.renderer.addSprite(spr);
            env.addEntity(spr);
            
            Element * e = new Element();
            e->type(type);
            e->setSprite(spr);
            
            s->setElement(e);
        }
    }
}

void Board::setPosition(int px, int py)
{
    //CTU: Translate all the slots
    for(SlotContainer::iterator it = slots.begin(); it != slots.end(); ++it)
    {
        Slot * s = (*it);
        Rect r = s->AABB();
        r.x += px;
        r.y += py;
        s->AABB(r);
    }
}

Slot * Board::getSlotAt(int x, int y)
{
    for(SlotContainer::iterator it = slots.begin(); it != slots.end(); ++it)
    {
        if( (*it)->AABB().pointIn(x, y) )
        {
            return *it;
        }
    }
    
    return NULL;
}