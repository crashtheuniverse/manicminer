//
//  TextureManager.h
//  ManicMiner
//
//  Created by CrashTheuniversE on 24/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#ifndef ManicMiner_TextureManager_h
#define ManicMiner_TextureManager_h

#include <string> 
#include <SDL2/SDL.h>
#include <map>
#include "Texture.h"

namespace Engine {
    
    struct Environment;
    
    class TextureManager
    {
        typedef std::map<std::string, Texture*> TextureContainer;
        TextureContainer textures;

        Environment& env;
        
        std::string basePath;
        
    public:
        
        TextureManager(Environment& e) : env(e) {};
        ~TextureManager();
        
        void setPath(const std::string& p) { basePath = p; };
        
        Texture * getTexture(const std::string& filename);
    };
}

#endif
