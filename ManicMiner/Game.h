//
//  Game.h
//  ManicMiner
//
//  Created by CrashTheuniversE on 23/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#ifndef __ManicMiner__Game__
#define __ManicMiner__Game__

#include "Entity.h"

#include "Sprite.h"

class Board;

class Game : public Engine::Entity
{
    Board * board;
    Engine::Sprite * bg;
    
public:
    
    Game();
    ~Game();
    virtual void update(Engine::Environment& env);
    virtual void init(Engine::Environment& env);
    
public:
    
};

#endif /* defined(__ManicMiner__Game__) */
