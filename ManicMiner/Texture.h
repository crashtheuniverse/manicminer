//
//  Texture.h
//  ManicMiner
//
//  Created by CrashTheuniversE on 24/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#ifndef ManicMiner_Texture_h
#define ManicMiner_Texture_h

#include <SDL2/SDL.h> 

namespace Engine {
    
    class Texture {
        
        SDL_Surface * surface;
        SDL_Texture * texture;
      public:
    
        Texture(SDL_Surface * s, SDL_Texture * t) : surface(s), texture(t) {};
        
        SDL_Texture * geTexture() const { return texture; };
        SDL_Surface * getSurface() const { return surface; };
    };
}



#endif
