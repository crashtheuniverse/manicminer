//
//  Element.cpp
//  ManicMiner
//
//  Created by CrashTheuniversE on 24/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#include "Element.h"
#include "Environment.h"
#include "Vec3.h"

const float PIXEL_PER_SECOND = 100.0f;

/////////////////////////////////////////////////////// METHODs

void Element::setPosition(const Engine::Vec3& pos)
{
    sprite->position(pos);
}

void Element::moveTo(const Engine::Vec3& dest)
{
    sprite->moveTo(dest);
}