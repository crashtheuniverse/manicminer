//
//  Entity.h
//  ManicMiner
//
//  Created by CrashTheuniversE on 24/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#ifndef ManicMiner_Entity_h
#define ManicMiner_Entity_h

namespace Engine {

    struct Environment;
    
    class Entity {
        public:
        virtual void init(Environment& env) {};
        virtual void update(Environment& env) {};
        virtual ~Entity() = 0;
    };
    
    inline Entity::~Entity() {};
}

#endif
