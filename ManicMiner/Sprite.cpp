//
//  Sprite.cpp
//  ManicMiner
//
//  Created by CrashTheuniversE on 23/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#include "Sprite.h"
#include <SDL2/SDL.h>
#include "Texture.h"
#include "Environment.h"
#include <algorithm>

const float SPEED = 0.25f;

namespace Engine {
    
    bool animComplete(Vec3AnimController*a) {
        
        if(a->complete()) {
            delete a;
            return true;
        }
        
        return false;
    }
    
    bool animDestroy(Vec3AnimController *a) {
        delete a;
        return true;
    }
    
    void Sprite::update(Engine::Environment& env)
    {
        for(AnimContainer::iterator it = animations.begin(); it != animations.end(); ++it)
        {
            (*it)->update(env.deltaTime);
        }
        animations.erase(std::remove_if(animations.begin(), animations.end(), animComplete), animations.end());
    }

    void Sprite::setTexture(Texture * t)
    {
        texture = t;
        width = texture->getSurface()->w;
        height = texture->getSurface()->h;
    }
    
    void Sprite::moveTo(const Vec3& d)
    {
        animations.erase(std::remove_if(animations.begin(), animations.end(), animDestroy), animations.end());
        Vec3AnimController * anim = new Vec3AnimController(pos, d, SPEED);
        animations.push_back(anim);
    }
}
