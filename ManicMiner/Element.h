//
//  Element.h
//  ManicMiner
//
//  Created by CrashTheuniversE on 24/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#ifndef __ManicMiner__Element__
#define __ManicMiner__Element__

#include "Entity.h"
#include "Sprite.h"


class Element : public Engine::Entity {

    Engine::Sprite * sprite;
    int blockType;
    
public:
    
    Element() : sprite(NULL) {};
    virtual ~Element() {};
    
    void moveTo(const Engine::Vec3& dest);
    void setPosition(const Engine::Vec3& pos);
    
    Engine::Sprite * getSprite() const { return sprite; };
    void setSprite(Engine::Sprite * s) { sprite = s; };
    
    void type(int t) { blockType = t; };
    int type() const { return blockType; };
    
    bool isAnimating() {
        if(sprite != NULL)
            return sprite->isAnimating();
        
        return false;
    }
};

#endif /* defined(__ManicMiner__Element__) */
