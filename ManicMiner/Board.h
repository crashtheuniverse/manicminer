//
//  Board.h
//  ManicMiner
//
//  Created by CrashTheuniversE on 23/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#ifndef __ManicMiner__Board__
#define __ManicMiner__Board__

#include "Entity.h"
#include "Slot.h"
#include <vector>
#include "Sprite.h"

class Board : public Engine::Entity
{
    typedef std::vector<Slot*> SlotContainer;

    SlotContainer slots;
    SlotContainer slotsToDestroy;
    
    int width;
    int height;
    int x, y;
    int blockSize;
    int maxTypes;
    
    Engine::Sprite * boundingSprite;
    
    Slot * srcSlot;
    Slot * dstSlot;
    
    bool swapBack;
    bool swap;
    
    void spawnEmpty(Engine::Environment& env);
    void dragDown();
    
    void calcSlotsToDestroy(Slot * slot);
    void calcSlotsToDestroyMatch3();
    bool destroySlots();
    
    int rowSlotsToDestroy(int start, int row, SlotContainer& toDestroy);
    int columnSlotsToDestroy(int start, int col, SlotContainer& toDestroy);
    void pushSlotsToDestroy(SlotContainer& toDestroy);
    
    bool hasElementsInMovement();
    
public:
    
    Board(int w, int h, int blocksize, int types);
    virtual ~Board() {};
    virtual void init(Engine::Environment & env);
    virtual void update(Engine::Environment & env);
    
    void setPosition(int px, int py);
    
    Slot * getSlotAt(int px, int py);
    Slot * getSlot(int x, int y);
    
    void fill();
    
    bool hasEmptySlots(); 
};


#endif /* defined(__ManicMiner__Board__) */
