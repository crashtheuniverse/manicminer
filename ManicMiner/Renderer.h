//
//  Renderer.h
//  ManicMiner
//
//  Created by CrashTheuniversE on 23/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#ifndef __ManicMiner__Renderer__
#define __ManicMiner__Renderer__

#include <SDL2/SDL.h>
#include <list>
#include "Sprite.h"

namespace Engine {
    
    typedef std::list<Sprite*> SpriteContainer;
    
    class Renderer {
        
        SDL_Window* window;
        SDL_Renderer* renderer;
        
        SpriteContainer sprites;
        
    public:
        
        Renderer();
        ~Renderer();
        
        int init(int w, int h);
        
        void renderFrame();
        void swap();
        
        void addSprite(Sprite* s) { sprites.push_back(s); };
        void removeSprite(Sprite * s) { sprites.remove(s); };
        
        SDL_Renderer * getRendererImp() { return renderer; };
    };
    
}

#endif /* defined(__ManicMiner__Renderer__) */
