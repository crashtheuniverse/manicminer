//
//  Environment.h
//  ManicMiner
//
//  Created by CrashTheuniversE on 24/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#ifndef ManicMiner_Environment_h
#define ManicMiner_Environment_h

#include <list>
#include <vector>
#include <string>
#include "Renderer.h"
#include "Entity.h"
#include <SDL2/SDL.h>

#include "TextureManager.h"

namespace Engine {
    
    struct Environment {

        typedef std::list<Entity*> EntityContainer;
        typedef std::vector<SDL_Event> EventContainer;
        
        Environment() : quit(false), textureManager(NULL) {};
        ~Environment() {
            //CTU: Destroy the list objects
        }
        
        //CTU: Env State
        float deltaTime;
        bool quit;
        Renderer renderer;
        
        EntityContainer entities;
        EventContainer  inputEvents;
        
        void addEntity(Entity * e) { entities.push_back(e); };
        void removeEntity(Entity * e) { entities.remove(e); };
        
        TextureManager * getTextureManager() {
            if(textureManager == NULL) {
                textureManager = new TextureManager(*this);
            }
            return textureManager;
        }
        
    private:
        
        TextureManager * textureManager;
    };
}


#endif
