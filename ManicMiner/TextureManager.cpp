//
//  TextureManager.cpp
//  ManicMiner
//
//  Created by CrashTheuniversE on 24/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#include "TextureManager.h"
#include <SDL2_image/SDL_image.h>
#include "Environment.h"


namespace Engine {
    
TextureManager::~TextureManager()
{
}

Texture * TextureManager::getTexture(const std::string& filename)
{
    TextureContainer::iterator it = textures.find(filename);
    if( it != textures.end() )
    {
        return it->second;
    }
    
    std::string fullPath = basePath + filename;
    
    SDL_Surface * s = IMG_Load(fullPath.c_str());
    SDL_Texture * t = SDL_CreateTextureFromSurface(env.renderer.getRendererImp(), s);
    Texture * tex = new Texture(s, t);
    
    if(t != NULL)
    {
        textures[filename] = tex;
    }
    
    return tex;
}
    
}
