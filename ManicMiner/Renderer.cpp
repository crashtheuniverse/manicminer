//
//  Renderer.cpp
//  ManicMiner
//
//  Created by CrashTheuniversE on 23/06/2013.
//  Copyright (c) 2013 CrashTheuniversE. All rights reserved.
//

#include "Renderer.h"
#include "Texture.h"

namespace Engine {
    
    Renderer::Renderer() : window(NULL), renderer(NULL)
    {
    }
    
    Renderer::~Renderer() {
    }
    
    
    int Renderer::init(int w, int h)
    {
        // Initialize SDL.
        if (SDL_Init(SDL_INIT_VIDEO) < 0)
            return 1;
        
        // Create the window where we will draw.
        window = SDL_CreateWindow("SDL_RenderClear",
                                  SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                  w, h,
                                  SDL_WINDOW_SHOWN);
        
        renderer = SDL_CreateRenderer(window, -1, 0);
        
        return 0;
    }
    
    void Renderer::renderFrame()
    {
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
        
        for(SpriteContainer::iterator it = sprites.begin(); it != sprites.end(); ++it)
        {
            Sprite * s = (*it);
            SDL_Rect r;
            Engine::Rect ori = s->AABB();
            r.x = ori.x; r.y = ori.y; r.w = ori.w; r.h = ori.h;
            
            Texture * t = s->getTexture();
            
            if(t != NULL)
            {
                SDL_RenderCopy(renderer, t->geTexture(), NULL, &r);
            }
            else
            {
                SDL_RenderDrawRect(renderer, &r);
            }
        }
    }
    
    void Renderer::swap()
    {
         SDL_RenderPresent(renderer);
    }
}

